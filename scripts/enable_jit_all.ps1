$allResourceGroups = (Get-AzResourceGroup).ResourceGroupName
$ports=(@{
            number=22;
            protocol="*";
            allowedSourceAddressPrefix=@("*");
            maxRequestAccessDuration="PT3H"
         },
        @{
            number=3389;
            protocol="*";
            allowedSourceAddressPrefix=@("*");
            maxRequestAccessDuration="PT3H"
        }
    )
foreach ($rg in $allResourceGroups) {
    $locations = (Get-AzVm -ResourceGroup $rg).Location | Sort-Object | Get-Unique
    foreach ($location in $locations) {
        $vms = (Get-AzVm -ResourceGroup $rg | Where-Object {$_.Location -eq $location}) | select Id
        $vms  | Foreach-Object {$_ | Add-Member -MemberType NoteProperty -Name "ports" -Value $ports}
        Set-AzJitNetworkAccessPolicy -Kind "Basic" -Location $location -Name "default" -ResourceGroupName $rg -VirtualMachine $vms
    }
}